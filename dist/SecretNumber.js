"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class SecretNumber {
    constructor(secretLength = 4) {
        this.secretLength = secretLength;
        this._secret = SecretNumber.secretNumber(this.secretLength);
    }
    get secret() {
        return this._secret;
    }
    static checkDistinctNumber(secret) {
        const str = secret + "";
        const test = [];
        test.push(str[0]);
        for (let i = 1; i < str.length; i++) {
            if (test.includes(str[i])) {
                return true;
            }
            else {
                test.push(str[i]);
            }
        }
        return false;
    }
}
exports.default = SecretNumber;
SecretNumber.secretNumber = (secretLength) => {
    let secret;
    const max = 10 ** secretLength - 1;
    const min = 10 ** (secretLength - 1);
    do {
        secret = Math.floor(Math.random() * max);
    } while (SecretNumber.checkDistinctNumber(secret) || secret < min);
    return secret;
};
