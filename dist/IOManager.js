"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
const prompt_1 = __importDefault(require("prompt"));
class IOManager {
}
exports.default = IOManager;
_a = IOManager;
IOManager.askForNumber = () => __awaiter(void 0, void 0, void 0, function* () {
    const { attemp } = yield prompt_1.default.get(["attemp"]);
    return attemp;
});
IOManager.outputResultTry = (result) => {
    console.log(`${result.bulls} bulls
${result.cows} cows`);
};
IOManager.outputResult = (result, secret) => {
    if (result === "WIN") {
        console.log("YOU WIN! ", secret);
        return;
    }
    if (result === "LOSE") {
        console.log("YOU LOST! ", secret);
        return;
    }
};
IOManager.outputMessage = (message) => {
    console.log(message);
};
