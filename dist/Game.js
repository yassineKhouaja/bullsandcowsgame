"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const IOManager_1 = __importDefault(require("./IOManager"));
const SecretNumber_1 = __importDefault(require("./SecretNumber"));
class Game {
    constructor() {
        this.secret = new SecretNumber_1.default(Game.secretLength).secret;
    }
    star() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                IOManager_1.default.outputMessage("Game start:");
                for (let index = 0; index < Game.maxTries; index++) {
                    IOManager_1.default.outputMessage("please give a number:");
                    const attemp = yield IOManager_1.default.askForNumber();
                    const result = Game.checkAttemp(String(attemp), this.secret);
                    IOManager_1.default.outputResultTry(result);
                    if (result.bulls === Game.secretLength) {
                        IOManager_1.default.outputResult("WIN", this.secret);
                        return;
                    }
                }
                IOManager_1.default.outputResult("LOSE", this.secret);
            }
            catch ({ message }) {
                IOManager_1.default.outputMessage(String(message));
            }
        });
    }
}
Game.secretLength = 4;
Game.maxTries = 10;
Game.checkAttemp = (attemp, secret) => {
    const strAttemp = attemp + "";
    const strSecret = secret + "";
    let cows = 0;
    let bulls = 0;
    for (let i = 0; i < strSecret.length; i++) {
        if (strSecret.includes(strAttemp[i])) {
            strSecret[i] === strAttemp[i] ? bulls++ : cows++;
        }
    }
    return { bulls, cows };
};
exports.default = Game;
