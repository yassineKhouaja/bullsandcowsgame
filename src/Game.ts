import { IGame } from "./Interface";
import IOManager from "./IOManager";
import SecretNumber from "./SecretNumber";

class Game implements IGame {
  private secret: number;
  static secretLength = 4;
  static maxTries = 10;
  constructor() {
    this.secret = new SecretNumber(Game.secretLength).secret;
  }

  static checkAttemp = (
    attemp: string,
    secret: number
  ): { bulls: number; cows: number } => {
    const strAttemp = attemp + "";
    const strSecret = secret + "";
    let cows = 0;
    let bulls = 0;

    for (let i = 0; i < strSecret.length; i++) {
      if (strSecret.includes(strAttemp[i])) {
        strSecret[i] === strAttemp[i] ? bulls++ : cows++;
      }
    }
    return { bulls, cows };
  };

  async star() {
    try {
      IOManager.outputMessage("Game start:");
      for (let index = 0; index < Game.maxTries; index++) {
        IOManager.outputMessage("please give a number:");
        const attemp = await IOManager.askForNumber();
        const result = Game.checkAttemp(String(attemp), this.secret);
        IOManager.outputResultTry(result);

        if (result.bulls === Game.secretLength) {
          IOManager.outputResult("WIN", this.secret);
          return;
        }
      }
      IOManager.outputResult("LOSE", this.secret);
    } catch ({ message }) {
      IOManager.outputMessage(String(message));
    }
  }
}

export default Game;
