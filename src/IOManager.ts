import prompt from "prompt";

export default class IOManager {
  static askForNumber = async () => {
    const { attemp } = await prompt.get(["attemp"]);
    return attemp;
  };

  static outputResultTry = (result: { bulls: number; cows: number }) => {
    console.log(`${result.bulls} bulls
${result.cows} cows`);
  };

  static outputResult = (result: string, secret: number) => {
    if (result === "WIN") {
      console.log("YOU WIN! ", secret);
      return;
    }

    if (result === "LOSE") {
      console.log("YOU LOST! ", secret);
      return;
    }
  };
  static outputMessage = (message: string) => {
    console.log(message);
  };
}
