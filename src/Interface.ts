export interface ISecretNumber {
  secret: number;
}

export interface IGame {
  star: () => void;
}
